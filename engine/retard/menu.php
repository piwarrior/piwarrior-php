<?php


class menuMaker {
    private $menu = [];
    private $selected = "";
    public function a ($link, $inside, $title){
        hC("css/menu.css");
        $this->menu[]=[
            "link"   => $link,
            "inside" => $inside,
            "title"  => $title
        ];
        return true;
    }
    public function add($link, $inside, $title){ 
        return $this->a($link, $inside, $title);
    }
    public function s($link){
        $this->selected=$link;
        return true;
    }
    public function select($link){
        return $this->s($link);
    }
    private function render (){
        return tE(
             'div',
             ['class'=>'menuLine'],
             tE(
                  'ul',
                  ['class'=>'menuHolder'], 
                  $this->renderPoints()
             )
        );
    }
    private function renderPoints(){
        $out = "";
        for ($i=0; count($this->menu)>$i; $i++)
            $out .= $this->renderPoint($i);
        return $out;
    }
    private function renderPoint($i){
        $class = "menuBlock";
        if($this->selected == $this->menu[$i]['link']) $class = "menuBlockSelected";
        return tE(
            'li', 
            ['class'=>$class], 
            tL(
                $this->menu[$i]["link"],
                $this->menu[$i]["inside"],
                $this->menu[$i]["title"],
                "menuLink"
            )); 
    }
   public function __destruct(){}
   public function html(){
        if(count($this->menu)>0){
            return $this->render();
        }
        return " "; 
    }
    public function g() {
       return $this->render();
    }
    public function get(){
        return $this->g();
    }
}


$menuManager = new menuMaker();


function tHa($link, $inside, $title) { 
    return $GLOBALS['menuManager']->a($link,  $inside, $title);
}



