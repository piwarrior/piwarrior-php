<?php


class form {
    private $form=[];
    private $inputs=[];
    private $html="";
    private $inside="";
    private function inputsBuild(){
        $this->inside="";
        foreach ($this->inputs as $i) 
            if($i["type"] == "select"){
                $this->inside.=$this->selectBuild($i);
            }else
                $this->inside.=$this->inputBuild($i);
        return $this->inside;
    }
    private function selectBuild($index){
        $options="";
        foreach($index['select'] as $k=>$i)
            $options.= tE('option', ['value'=>$k], $i); 
        unset($index['select']);
        return tE('p', [], tE('select', $index, $options));
    }
    private function inputBuild($index){
        return tE('p',[],tEs('input',$index));
    }
    public function buildThisCity(){
        $this->html=tE('form', $this->form, $this->inputsBuild());
    }
    public function c () {
        $this->inputs=[];
    }
    public function clean () {
        $this->c();
    }
    public function iB($placeholder = ""){
        $this->iA(["type"=>"submit", "value"=>$placeholder]);
    }
    public function inputSubmit($placeholder = ""){
        $this->iB($placeholder);
    }
    public function iH($name = "", $value = ""){
        $this->iA(["type"=>"hidden", "name"=>$name, "value"=>$value]);
    }
    public function inputHidden($name = "", $value = ""){
        $this->iH($name, $name);
    }
    public function iS($name = "", $list = [], $placeholder = ""){
        $this->iA(["type"=>"select", "name"=>$name, "select"=>$list, "placeholder"=>$placeholder]);
    }
    public function inputSelect($name = "", $list = [], $placeholder = ""){
        $this->iS($name, $list, $placeholder);
    }
    public function iA($options){
        $this->inputs[]=$options;
    }
    public function inputAdd($options){
        $this->iA($options);
    }
    public function addInput($options){
        $this->iA($options);
    }
    public function g(){
        $this->buildThisCity();
        return $this->html;
    }
    public function get(){
        return $this->g();
    }
    public function f($fields){
        for($i = 0; count($fields)>$i;$i++)
            $this->iA($fields[$i]);
        return $this->g();
    }
    public function fastForm ($fields){
        return $this->f($fields);
    }
    public function __construct($options){
        $this->form=$options;
    }
}



function tF($formType, $fields){
    $newForm = new form($formType);
    return $newForm->f($fields);
}


/*

   A form builder logikus ha megnezed a kodot.
   Eme logikussaga melle jonn egy egyszeru form epito.
   Ami egyenlore csak inputokat epit de e valtozni fog. 

   tF($formAtributumTomb, $inputMezokTombjetGyujtoTomb)


 */


