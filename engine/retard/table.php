<?php


class tableMaker {
    public $out = "";
    public $line = 0;
    public $rows = [];
    public function headeRs() {
        return tE('thead', [], tE('tr', [], $this->headerTh()));
    }
    public function headerTh(){
        $out = "";
        foreach ($this->rows as  &$value)
            $out .= tE('th', [], __($value));
        return $out;

    }
    public function body($bodyArray) {
        return tE('tbody', [], $this->lines($bodyArray));
    }
    public function lines ($bodyArray){
       $out = "";
       foreach ($bodyArray as &$arr)
            $out .= $this->line($arr);
       return $out;  
    }
    public function line($lineArray) {
        if ($this->line == 0) {
            $attr = ["class"=>"table_line0"];
            $this->line = 1;
        } else {
            $attr = ["class"=>"table_line1"];
            $this->line = 0;
        }
        return tE('tr', $attr, $this->lineRows($lineArray));
    }
    public function lineRows ($lineArray){
        $out="";
        foreach ($this->rows as  $value) {
            $out .= tE('td', [], $lineArray[$value]); 
        }
        return $out;
    }
    public function linkr($inputArr, $url, $field){
        $out=[];
        for($i = 0 ; count($inputArr)>$i;$i++){
            $line = [];
            foreach ($inputArr[$i] as $key => $value)
               $line[$key]='<a href="'. $url. $inputArr[$i][$field].'">'.$value.'</a>';
            $out[]=$line;
        }
        return $this->r($out);
    }
    public function render($inputArray) {
        return $this->r($inputArray);
    }
    public function r($inputArr) {
        $this->out = "";
        $this->rows = [];
        for ($i=0;count($inputArr)>$i;$i++)
            foreach ($inputArr[$i] as $key => $value)
                if (!in_array($key, $this->rows))
                    $this->rows[]=$key;
        $this->out .= $this->headeRs();
        $this->out .= $this->body($inputArr);    
        return tE('table', [], $this->out);;
    }

}

$tableManager = new tableMaker();


function tT ($inputArray) {
    return $GLOBALS['tableManager']->r($inputArray);
}

function tTo ($inputArray) {
    o(tT($inputArray));
}
function tTl ($inputArr, $url, $field){
    return $GLOBALS['tableManager']->linkr($inputArr, $url, $field);
}

function tTlo ($inputArr, $url, $field){
    o(tTl($inputArr, $url, $field));
}

/*

A tabla builder egyszeruen array el appellalo  html tabla keszito. 








miert ? 
Mert a symfony ba sokan hasznaljak a table bundle-t (https://packagist.org/packages/emc/table-bundle) es ugy gondoltam ez jol jonn egy ilyen mini projectbe.

*/



