<?php 

class elementMaker {
    private $separetor = '"';
    private function attributionFull($attr, $val){
        return " ".$attr. "=". $this->separetor. $this->attributionValue($val). $this->separetor;
    }
    private function attributionValue($val){
        $out = "";
        if (is_array($val))
            for ($i = 0 ; count($val)>$i;$i++){
                if ($i>0) $out.= " ";
                    $out.= $val[$i];
            }
        else 
           $out = $val;
        return $out;
    }
    public function title($title){
        return $this->m(
            'div', 
            ['class'=>'title'],
            $this->m(
                'h1',
                [],
                __($title)
            )
        );
    }
    public function subTitle($title){
        return $this->m(
            'div', 
            ['class'=>'title'],
            $this->m(
                'h3',
                [],
                __($title)
            )
        );
    }
    public function make($element, $attributions=[], $inside = ''){
        $this->m($element, $attributions, $inside);
    }
    public function m ($element, $attributions=[], $inside = ''){
        $out = "<".$element;
        foreach ($attributions as $attr => $val)
            $out.= $this->attributionFull($attr, $val);
        return $out. ">". $inside."</". $element. ">";
    }
    public function single ($element, $attributions){
       return $this-s($element, $attributions, $inside);
    }
    public function s ($element, $attributions){
        $out = "<".$element;
        foreach ($attributions as $attr => $val)
            $out.= $this->attributionFull($attr, $val);
        return $out. "/>";
    }
}

$element=new elementMaker();
function tE($element, $attributions=[], $inside = ''){
    return $GLOBALS['element']->m($element, $attributions, $inside);
}
function tEo($element, $attributions=[], $inside = ''){
    o(tE($element, $attributions, $inside));
}

function tEs($element, $attributions=[]){
    return $GLOBALS['element']->s($element, $attributions);
}

function tEso($element, $attributions=[]){
    o(tEs($element, $attributions));
}

function tEt($title){
    return $GLOBALS['element']->title($title);
}

function tEts($title){
    return $GLOBALS['element']->subTitle($title);
}

function tEto($title){
    o($GLOBALS['element']->title($title));
}

function tEtso($title){
    o($GLOBALS['element']->subtitle($title));
}

/*

Az element Maker kulonbozo html tagok lettre hozasat szolgalja. Egyfajta szerver oldali RealJs nek is tekintheto.De annak komplexitasanak a kozeleben sincs termeszetesen. 
A lenyeg az hogy a html kod ne legyen kozvetlenul jelen a php kodunkba. Ez alltal a kodunk maradjon jol olvashato, ertelmezheto emberi lenyek szamara. 

2 fele elem epites van. Multi es single. 
A multi tartalmaz mas elemeket (pl div nav table p stb) 
A single nem tartalmaz mas elemeket (pl. br input img stb)

Termeszetesen itt is van roviditett forma. 
a tE a multi elem epito.
tE($elem $attributumTomb, $belsoElemek);

A tEs a single elem epito  
tEs($elem $attributumTomb);








*/

