<?php


function curler($url, $postfield) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "". $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfield));
    if (curl_errno($ch))
        return debug(' Curler Unable to connect: ' . curl_error($ch), 6);
    $out = curl_exec($ch);
    if (curl_errno($ch))
        return debug(' Curler Unable to connect: ' .  curl_error($ch), 7);
    curl_close($ch);
    debug(' Curler : || '.  $out. ' || '. json_encode($postfield), 8);
    return json_decode($out, true);
}

