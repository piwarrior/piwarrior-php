<?php
#NS   ..|ctl
#use  E|kernel.cmd


class clCommands { 
    private $command;
    private $options;
    private $commands; 
    private $registered;
    private $model;
    public function register ($command, $options){
        $this->command=$command;
        $this->options=$options;
    }
    public function add($commands, $func, $modul=''){
        $this->commands[$commands[0]]=$func;
        for($i =0 ;count($commands) >$i; $i++){
            if($this->registered == $this->commands)
                $this->register($commands[0]);
        }
    }
    
    public function run(){
	if ($this->model != '')
            include(mL($this->model));
        $this->commands[$this->command]($this->options);
    }
    public function __construct(){
        $this->commands=[];
    }
}

$cLcommands = new clCommands();
$cLcommands->register($argv[1], []);
function command ($command, $func){
    global $cLcommands;
    return $cLcommands->add($command, $func);
}

command(['hello'], function($in){
    tRa('hello');
    tMa('hello');
    o(tR());
    o(tM());
    o('hello');
    o(tT(['hello'=>'hello']));
});




//for($modelI=0; count($modelsList) > $modelI ; $modelI)
//    include($modelManager->modelDir.'/cli.php');

