<?php
#NS E|T|test

#use E|kenel 

abstract class testAbstract {
     private $result = [];
     private $name = "default";
     /*
           

     */
     public function timeCalc($start, $end){
         return floor(($end-$start)*1000000);
     }
     public function testRun($name, $goal, $test){
         $start  = microtime(true);
         $result = $test();
         $end    = microtime(true);
         $this->resultAdd($name, $goal, $result, $this->timeCalc($start, $end)); 
     }
     public function resultAdd($name, $goal, $result, $interval){
         $this->result[]=[
             "name"    =>$name,
             "goal"    =>$goal,
             "result"  =>$result,
             "interval"=>$interval
         ];
     }
     public function resultRender(){
         $out = "";
         foreach ( $this->result as &$tagek){
             $out .= "  ". $tagek['name']. " ¦ ";
             if ($tagek['goal'] === $tagek['result']){
                 $out .= "ok";
             }else{
                 $out .= "failed";
             }
             $out .=" ¦ ". $tagek['interval']. " ¦ \n";
         }
         $out .= ""; 
         o($out);

     }
     public function resultRenderHtml(){
         $out = "<table>";
         foreach ( $this->result as &$tagek){
             $out .= "<tr><td>". $tagek['name']. "</td><td>";
             if ($tagek['goal'] === $tagek['result']){
                 $out .= "ok";
             }else{
                 $out .= "failed";
             }
             $out .="</td><td>". $tagek['interval']. " μs</td></tr>";
         }
         $out .= "</table>"; 
         o($out);
     }
     private function selfTest(){
         $this->testRun(
              $this->name. " test start", 
              '',
              function () { return '';}

          );

     }

     public function  __construct(){
         $this->selfTest();
         return $this->init();
     }

}


