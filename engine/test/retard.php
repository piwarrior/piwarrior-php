<?php

class retardTestClass extends testAbstract {
     private $name = "Retard toold";
     public function retardElement (){
          $this->testRun(
              "retard element", 
              '<div class="testClass" id="testId">test<br/></div>',
              function () { return tE('div', ['class' => 'testClass', 'id'=>'testId'], 'test'.tEs('br', []));}

          );
     }
     public function retardTable(){
          $testGoal    = '<table>';
          $testGoal   .= '<thead><tr><th>columA</th><th>columB</th></tr></thead>';
          $testGoal   .= "<tbody>";
          $testGoal   .= '<tr class="table_line0"><td>cAl1</td><td>cBl1</td></tr>';
          $testGoal   .= '<tr class="table_line1"><td>cAl2</td><td>cBl2</td></tr>';
          $testGoal   .= "</tbody>";
          $testGoal   .= "</table>";
          $testArray   =[];
          $testArray[] =[
              "columA" => "cAl1",
              "columB" => "cBl1"
          ];
          $testArray[] =[
              "columA" => "cAl2",
              "columB" => "cBl2"
          ];
          $time_start  = microtime(true);
          $testResult  = tT($testArray);
          $time_end    = microtime(true);
          $this->resultAdd("retard table", $testGoal, $testResult, $this->timeCalc($time_start, $time_end));
     }
     public function retardForm (){
          $testGoal    = '<form method="Post">';
     //     $testGoal   .= '<p>testPlace</p>';
          $testGoal   .= '<p><input type="text" name="testName" placeholder="testPlace"/></p>';
          $testGoal   .= '</form>';
          $time_start = microtime(true);
          $testResult = tF([
                   "method"=>"Post"
               ],[
                   [
                        "type"        => "text",
                        "name"        => "testName",
                        "placeholder" => "testPlace"
                   ]
               ]);
          $time_end   = microtime(true);          
          $this->resultAdd("retard form", $testGoal, $testResult, $this->timeCalc($time_start,$time_end));
     }
     public function init(){
          $this->retardElement();
          $this->retardTable();
          $this->retardForm();
          $this->resultRenderHtml();
     }

}

$retardTest = new retardTestClass();