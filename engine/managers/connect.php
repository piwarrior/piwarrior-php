<?php


class connectManagerClass extends abstractSQL {
    private $id;
    private $sql;
    private function check () { 
        $this->id=sG('connectId');
    }
    private function create () { 
//        $this->init();
        $this->id = $this->getId('connectId');
        $this->qf('connectAdd', [$this->id, time(), $GLOBALS['clientManager']->getClientId(), $_SERVER['SERVER_ADDR'], 0]);
        sS('connectId', $this->id);
    }
    public function getConnectId(){
        return $this->id;
    }
    public function init(){
        $this->check();  
        if($this->id == false) $this->create();
    }
}

$connectManager = new connectManagerClass();

