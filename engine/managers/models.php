<?php 


class modelsManagerClass {
    private $outputMode;
    private $modelsLoaded=[];
    private $emptyModel='';
    private $modelDir="../models/";
    public function mode (){
        return $this->outputMode;
    }
    public function load ($model){
        hD('model', 'load : '. $model);
        return ($this->modelDir. $model.'/include/function.php');
    }
    public function view ($model,$view){
        hD('model', 'view : '.$model.'/'.$view);
        include($this->modelDir.$model.'/view/'.$view.'.php'); 
    }
    private function modelRoute(){
        if($this->mode() == "html")
            return '/model';
        if($this->mode() == "json")
            return '/model.api';
    }
    private function inOut(){
        if(sA()) 
            return '.admin.php';
        if(!($GLOBALS['loginManager']->getStatus())) 
            return '.outside.php';
        if($GLOBALS['loginManager']->getStatus())
            return '.inside.php';
        return '.outside.php';
    }
    public function run ($model){
        hD('model', 'run : '. $model);
        return (
            $this->modelDir. 
            $model.
            $this->modelRoute().
            $this->inOut()
        );
   }
   public function api ($model){
       return ($this->modelDir. $model. '/api.php');
   }
   public function __construct() {
      $this->outputMode = "html";
      if(iC('api')) 
         $this->outputMode = "json";
   }
   public function init() {}
   public function __destruct(){}
}


$modelsManager= new modelsManagerClass();


function mL($model){
   return $GLOBALS['modelsManager']->load($model);
}


function mV($model, $view){
   return $GLOBALS['modelsManager']->view($model, $view);
}


function mR($model){
   return $GLOBALS['modelsManager']->run($model);
}

function mA($model){
   return $GLOBALS['modelsManager']->api($model);
}



