<?php 

class loginManagerClass extends abstractSQL{
     private $id = 0;
     private $status = false;
     private $sql;
     private function check(){
         if(sG('loginId') == false)
             return false;
         $userId=$this->qf(
             'userLoginedCheck', 
             [sG('loginId'), 
             sG('clientId'),
             session_id()
         ]);
         if($userId==false) 
             return false;
         $this->status=true;
         sS('userId', $userId);
     }
     public function logout (){
         if($this->status == false) return false;
         $this->qf('userLogout', [sG('loginId')]);
         sS('loginId', false);
         sS('userId', false);
         return true;
     }
     public function getLoginId(){
         return $this->id;
     }
     public function getStatus(){
         return $this->status;
     }
     public function init(){
         $this->check();
         return $this->status;
     } 
}


$loginManager = new loginManagerClass();




