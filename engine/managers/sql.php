<?php

abstract class abstractSQL {
    public $DBM;// = $GLOBALS['DBM'];
    public $config;// = $GLOBALS['DBM']->config;
    public $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM01234567890";
    public $time;// = $GLOBALS['DBM']->time;
    private $db;
    public function timeStamp() {
        return round(microtime(true));
    }
    public function getSalt($salt = 1) {
        $o = "";
        for ($i = 0; $salt > $i; $i++)
            $o .= substr($this->chars, rand(0, strlen($this->chars)), 1);
        return $o;
    }
    public function passwordHash($password) {
        return hash('sha512', hash('sha1', hash('sha512', hash('sha1', $password))));
    }
    public function insert(){}
    public function update(){}
    public function select(){}
    public function query($query) {
        return $res = $this->DBM->query($query);
    }
    public function justquery($query) {
        return $res = $this->DBM->justquery($query);
    }
    public function procedurequery($procedure, $varArray){
        return $this->qp($procedure, $varArray);
    }
    public function qp($functione, $varArray = []){
        $sql = "`". $this->db ."`.`". $functione. "` ("; 
        for($i=0;count($varArray)>$i;$i++){
            $this->security($varArray[$i]);
            if($i>0) $sql.=", ";
            $sql.="'".$varArray[$i]."'";
        }
        $sql.=')';
        hD('sql', ' CALL '. $sql, []);
        return $this->query("CALL ". $sql);
    }
    public function functionquery($procedure, $varArray){
        return $this->qf($procedure, $varArray);
    }
    public function qf($procedure, $varArray=[]){
        $sql = "`". $this->db ."`.`". $procedure. "`("; 
        for($i=0;count($varArray)>$i;$i++){
            $this->security($varArray[$i]);
            if($i>0) $sql.=", ";
            $sql.="'".$varArray[$i]."'";
        }
        $sql.=')';
        hD('sql', ' SELECT '. $sql);
        return $this->query("SELECT ". $sql)[0][$sql];
    }
    public function selectquery($otherHalf){
        return $this->qs($otherHalf);
    }
    public function qs($otherHalf){
        $sql = "SELECT '". $this->db. "'.". $otherHalf;
        return $this->query($sql);
    }
    public function security($string) {
        return htmlspecialchars($string, ENT_QUOTES);
    }
    public function getId($name) { // inside serializer. Better if we do with care
        $name = $this->security($name);
        $sql = "SELECT ". $this->config['dbs']['serial']. ".`getId`('". $name. "') AS `id`";
        $result=$this->query($sql);
        return $result[0]['id'];
    }
     public function getSerial($name) { // inside serializer. Better if we do with care
        return $this->gS($name);
     }
     public function gS($name) { // inside serializer. Better if we do with care
        $name = $this->security($name);
        $sql = "SELECT ". $this->config['dbs']['serial']. ".`getSerial`('". $name. "') AS `serial`";
        $result=$this->query($sql);
        return $result[0]['serial'];
    }
    public function outputArrayListToObject ($array, $id, $val){
          return $this->oATO($array, $id, $val);
    }
    public function oATO ($array, $id, $val){
          $out=[];
          foreach($array as $k=>$i){
              $out[$i[$id]]=$i[$val];
          }
          return $out;
    }

    public function __construct() {
        $this->DBM = $GLOBALS['DBM'];
        $this->config = $GLOBALS['DBM']->config;
        $this->db= $this->config['db'] ;
        $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM01234567890";
        $this->time = $GLOBALS['DBM']->time;
        $this->init();
    }
    public function use($db){
        $this->db=$db;
    }
    public function init() {
    }
    public function __destruct(){}
}



class connectSQL {
    public $sql;
    public $config;
    public $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM01234567890";
    public $time = "";
    public function timeStamp() {
        return round(microtime(true));
    }
    public function getSalt($salt = 1) {
        $o = "";
        for ($i = 0; $salt > $i; $i++)
            $o .= substr($this->chars, rand(0, strlen($this->chars)), 1);
        return $o;
    }
    public function query($query) {
        $rows = [];
        $res = $this->sql->query($query);
        if ($this->sql->errno)
            hD('error', "QUERY : || " . $query . " || " . $this->sql->error, 6);
        if ($res == false)
            return [];
        while ($row = $res->fetch_assoc())      
            $rows[] = $row;
        $this->sql->close();
        $this->connect();
        return $rows;
    }

    public function justquery($query) {
        $res = $this->sql->multi_query($query);
        if ($this->sql->errno)
            hD('error', "JUSTQUERY : !!  " . $query . " !! " . $this->sql->error, 6);
    }
    public function security($string) {
        return htmlspecialchars($string, ENT_QUOTES);
    }
    protected function disconnect(){
        $this->sql->cloase();
        $this->connect();
    }
    protected function connect() {
        $this->sql = new mysqli('127.0.0.1', $this->config['user'], $this->config['password'], $this->config['db']);
        if ($this->sql->connect_errno)
            die("die die die fucking process just died!");
    }
    public function __construct() {
        global $super_config;
        $this->config = $super_config['sql'];
        $this->connect();
        $this->time = time();
        $this->init();
    }
    public function init() {
    }
    public function __destruct() {}
}

$DBM= new connectSQL();

function qU($db){
    $GLOBAL['DBM']->use($db);
}


function qF($fun, $var){
    return $GLOBAL['DBM']->qf($fun, $var);
}

function qS(){
    return $GLOBAL['DBM']->qs($half);
}





