<?php 


class modelsManagerClass {
    private $modelsLoaded=[];
    private $emptyModel='';
    private $modelDir="../models/";
    public function load ($model){
        hD('model', 'load : '. $model);
        return ($this->modelDir. $model.'/include/function.php');
    }
    public function view ($model,$view){
        hD('model', 'view : '.$model.'/'.$view);
        include($this->modelDir.$model.'/view/'.$view.'.php'); 
    }
    public function run ($model){
        hD('model', 'run : '. $model);	
        return ($this->modelDir. $model. '/model.cli.php');
   }
    public function api ($model){
        return ($this->modelDir. $model. '/api.php');
    }
    public function __construct() {}
    public function init() {}
    public function __destruct(){}
}


$modelsManager= new modelsManagerClass();


function mL($model){
   return $GLOBALS['modelsManager']->load($model);
}


function mV($model, $view){
   return $GLOBALS['modelsManager']->view($model, $view);
}


function mR($model){
   return $GLOBALS['modelsManager']->run($model);
}

function mA($model){
   return $GLOBALS['modelsManager']->api($model);
}



