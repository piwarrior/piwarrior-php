<?php

abstract class renderEngineAbstract {
      private $flow=[];
      private $debugArray = [];
      private $title="Pandora";
      private $cdns = "";
      private $headerCss=[];
      private $menu = "";
      private $headerJs=[];
      private $bodyHtml="";
      private $headerHtml="";
      public function debug($category, $data){}
      public function addData ($tag, $data){}
      public function cssToHeader($cssToAdd){}
      public function jsToHeader($jsToAdd){}
      public function toHeader ($tag){}
      public function setTitle($title){}
      public function toBody ($tag){}
      public function renderHead (){}
}

class renderEngineClassCL extends renderEngineAbstract {
      private $flow=[];
      private $debugArray = [];
      private $title="Pandora";
      private $cdns = "";
      private $headerCss=[];
      private $menu = "";
      private $headerJs=[];
      private $bodyHtml="";
      private $headerHtml="";
      public function debug($category, $data){
          if(!isset($this->debugArray[$category])) $this->debugArray[$category]=[];
          $this->debugArray[$category][]=$data;
      }
      public function debugVar($category, $var, $val){
          if(!isset($this->debugArray[$category])) $this->debugArray[$category]=[];
          $this->debugArray[$category][]=$var.' : '. $val;
      }
      public function debugVarList($category, $list){
          foreach ($list as $var => $val)
              $this->debugVar($category, $var, $val); 
      }
      private function debugRender(){
          $out="";
          foreach ($this->debugArray as $key => $value)$out.=$key."\n=========\n".$this->debugRenderEach($value);
      }
      private function debugRenderEach($array){
          $out= " ";
          foreach($array as $each) $out .= $each;
          return $out;
      }
      public function addData ($tag, $data){
          $flow[$data]=$tag;
      }
      public function toBody ($tag){
          $this->bodyHtml.=$tag."\n";
      }
      public function render(){
          echo($this->bodyHtml);
      }
      public function __construct(){
      }
}

class renderEngineClassJson extends renderEngineAbstract {
      private $flow=[];
      public function addData ($tag, $data){
          $this->flow[$tag]=$data;
      }
      public function __construct(){
          header('Content-type:applicatio/json; charset=utf8');
      }
      public function __destruct (){}
      public function render(){
          echo(json_encode($this->flow, JSON_NUMERIC_CHECK));
      }
}


class renderEngineClassHtml extends  renderEngineAbstract {
      private $flow=[];
      private $debugArray = [];
      private $title="Pandora";
      private $cdns = "";
      private $headerCss=[];
      private $menu = "";
      private $headerJs=[];
      private $bodyHtml="";
      private $headerHtml="";
      private function cdn(){
          return "http://cdn.rrd.vm/";
      }
      publIc function debug($category, $data){
          hC("css/debug.css");
          $this->jsToHeader("js/debug.js");
          if(!isset($this->debugArray[$category])) $this->debugArray[$category]=[];
          $this->debugArray[$category][]=$data;
      }
      public function debugVar($category, $var, $val){
          hC("css/debug.css");
          $this->jsToHeader("js/debug.js");
          if(!isset($this->debugArray[$category])) $this->debugArray[$category]=[];
          $this->debugArray[$category][]=$var.' : '. $val;
      }
      public function debugVarList($category, $list){
          foreach ($list as $var => $val)
              $this->debugVar($category, $var, $val); 
      }
      private function debugRender(){
          $out="";
          $menu="";
          foreach ($this->debugArray as $key => $value){
               $menu.=tE(
                   'div', 
                   ['class' => 'debugZoneMenuPoint'], 
                   tE(
                       'div', 
                       [
                           'class' => 'debugZoneMenuPointIn', 
                           'id'    => 'debugZoneMenuPoint'.$key
                       ], 
                       $key
                   )
               );
               $out.=tE('div', ['class' => 'debugZoneCategory'], 
                   tE('div', ['class'=>'debugZoneCategoryTitle'], $key).
                   $this->debugRenderEach($value)
               );
          }
          return tE('div', ['class'=>'debug', 'id'=>'debug'], 
              tE('div', ['class'=>'debugZoneMenu'], $menu).
              tE('div', ['class'=>'debugZoneBody'], $out)
         ).
         tE('div', ['class'=>'debugButton', 'id'=>'debugButton'], 'Debug');
      }
      private function debugRenderEach($array){
          $out = " ";
          foreach($array as $each) $out .= tE('div', ['class' => 'debugZoneCategoryLine'], $each);
          return $out;
      }
      public function cssToHeader($cssFile){
          if (
              (!isset($this->headerCss[0]))||
             (($cssFile != $this->headerCss[0]) && 
              NULL == array_search($cssFile, $this->headerCss))
              )
              $this->headerCss[] = $cssFile;
      }
      public function jsToHeader($jsFile){
          $search=array_search($jsFile, $this->headerJs);
          if (
              (!isset($this->headerJs[0]))||
              (($jsFile != $this->headerJs[0])&&
              ($search == NULL))
        )
              $this->headerJs[] = $jsFile;
      }
      public function toHeader ($tag){
          $this->headerHtml.=$tag;
      }
      public function setTitle($title){
          $this->title=$title;
      }
      public function toBody ($tag){
          $this->bodyHtml.=$tag;
      }
      public function toMenu ($menu){
          $this->menu=$menu;
      }
      public function renderHead (){
          $this->headerHtml = "<title>" . $this->title . "</title>";
          foreach ($this->headerJs as $i)
              $this->headerHtml .= '<script src="' . $this->cdn() . $i . '"></script>';
          foreach ($this->headerCss  as $i)
              $this->headerHtml .= '<link rel="stylesheet" type="text/css" href="'.$this->cdn() . $i . '"/>';
      }
      public function __construct(){
          header('Content-type:text/html; charset=utf8');
      }
      public function render (){
          $this->renderHead();
          echo(
               "<!DOCTYPE html><html><head><meta charset=" . '"utf-8" />' . 
               $this->headerHtml . "</head><body>"
          );
          echo($GLOBALS['menuManager']->html());
          echo(tR());
          echo(tM());
          echo($this->bodyHtml);
          if ($GLOBALS['super_config']['debug']){
              $this->debugVarList('session', sGa());
              echo(tE('div', ['class'=>'debugZone'], $this->debugRender()));
          }
          echo("</body></html>");
      }
      public function __destruct (){}
}

if($modelsManager->mode() == "html"){
    $renderEngine = new renderEngineClassHtml();
}elseif($modelsManager->mode() == "cli"){
    $renderEngine = new renderEngineClassCL();
}else{
    $renderEngine = new renderEngineClassJson();
}
function h($o) {
     $GLOBALS['renderEngine']->toHeader($o);
}

function hD($c, $d) {
     $GLOBALS['renderEngine']->debug($c, $d);
}

function hT($o) {
     $GLOBALS['renderEngine']->setTitle($o);
}

function hC($o) {
     $GLOBALS['renderEngine']->cssToHeader($o);
}

function hS($o) {
     $GLOBALS['renderEngine']->jsToHeader($o);
}

function hO() {
}

function o($o) {
     $GLOBALS['renderEngine']->toBody($o);
}

function oA($t, $d) {
     $GLOBALS['renderEngine']->addData($t, $d);
}

function endOut() {
     $GLOBALS['renderEngine']->render();
}

