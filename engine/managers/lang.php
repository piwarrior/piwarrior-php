<?php


$lang = [];
$langDir = "../lang/";

include ($langDir. 'hu.php');
if (isset($_SESSION)&&isset($_SESSION['LANG'])) 
if ($_SESSION['LANG'] == "HU") {
    include $langDir. 'hu.php';
}


if ($super_config['langLearn']) {

    function __($input) {
        global $lang;
        global $langLearned;
        global $langLearnNew;
        if (!in_array($input, $langLearned)) {

            $langLearned[] = $input;
            $langLearnNew[] = $input;
        }
        if (isset($lang[$input])) {
            return $lang[$input];
        } else {
            return $input;
        }
    }

} else {

    function __($input) {
        global $lang;
        if (isset($lang[$input])) {
            return $lang[$input];
        } else {
            return $input;
        }
    }

}

    
    
    
    
  