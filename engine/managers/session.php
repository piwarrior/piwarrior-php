<?php


class sessionManagerClass {
    private $session = [];
    private $lang="HU";
    private $reverse=['userId','lang'];
    private function startSession(){
        session_start();
        $this->session=$_SESSION;
        $_SESSION=[];
        $this->reverseReload();
    }
    public function getLang(){
        return $this->lang;
    }
    public function getAll(){
        return $this->session;
    }
    public function setLang($lang){
        $this->session['lang']=$lang;
        $this->lang=$lang;
    }
    public function set($index, $value){
        return $this->s($index, $value);   
    }
    public function reverseReload(){
        if (isset($this->session['lang'])) $this->lang=$this->session['lang'];
    }
    public function s($index, $value){
        $this->session[$index]=$value;
        if(in_array($index, $this->reverse)) $this->reverseReload();
    }
    public function g($name){
        if(!isset($this->session[$name])) return false;
        return $this->session[$name];
    }
    public function get($name){
        return $this->g($name);
    }
    public function __construct(){
        $this->startSession();
    }
    public function __destruct(){
        $_SESSION=$this->session;
    }
}

$sessioniManager = new sessionManagerClass();


function sG($index){
    return $GLOBALS['sessioniManager']->g($index);
}

function sS($index, $value){
    return $GLOBALS['sessioniManager']->s($index, $value);
}



function sGa(){
    return $GLOBALS['sessioniManager']->getAll();
}

function sA(){
    return false;
}


