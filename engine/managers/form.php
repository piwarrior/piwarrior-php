<?php

abstract class abstractForm {
    public $form;
    public $formClass;
    public $fields=[];
    public $html="";
    public $lists=[];
    public function fieldAdd($name, $value){
        hD('formAbstractClass', 'fieldAdd', $value);
        $this->fields[$name]=$value; 
    }
    public function fieldCheck($name){
        if(isset($this->fields[$name]))
            return true;
        return false;
    }
    public function fieldGet($name){
        return $this->fields[$name];
    }
    public function fieldText($name, $placeholder){
        $inputArray=["type"=>"text","name"=>$name, "placeholder"=>__($placeholder)]; 
        if($this->fieldCheck($name))
            $inputArray['value']=$this->fieldGet($name);
        return $this->form->iA($inputArray);
    }
    public function fieldList($name,$list,$placeholder){
        if(isset($this->lists[$list]))
            $this->form->iS($name, $this->lists[$list], __($placeholder));
    }
    public function listAdd($name, $list){
        hD('formAbstractClass', 'listAdd', $list);
        $this->lists[$name]=$list; 
    }
    public function listCheck($name){
        if(isset($this->lists[$name]))
            return true;
        return false;
    }
    public function listGet($name){
        return $this->lists[$name];
    }
    public function hidden($field){
        return $this->form->iH(
            $field,
            $this->fields[$field]
        );
    }
}

