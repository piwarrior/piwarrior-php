<?php

#NS E|M|tableCL

class tableMaker {
    public $out = "";
    public $line = 0;
    public $rows = [];
    private $rowsSize = [];
    public function headeRs() {
        return  $this->headerTh();
    }
    public function row($in){
        $size = strlen($in);
        return $in." | ";
    }
    public function headerTh(){
        $out = " | ";
        foreach ($this->rows as  &$value)
            $out .= $this->row(__($value->title));
        return $out;

    }
    public function body($bodyArray) {
        return  $this->lines($bodyArray);
    }
    public function lines ($bodyArray){
       $out = "";
       foreach ($bodyArray as &$arr)
            $out .= $this->line($arr);
       return $out;  
    }
    public function line($lineArray) {
        if ($this->line == 0) {
            $this->line = 1;
        } else {
            $this->line = 0;
        }
        return $this->lineRows($lineArray);
    }
    public function lineRows ($lineArray){
        $out="";
        foreach ($this->rows as  $value) {
            $out .= $lineArray[$value]; 
        }
        return $out;
    }

    public function render($inputArray) {
        return $this->r($inputArray);
    }
    public function r($inputArr) {
        $this->out = "";
        $this->rows = [];
        for ($i=0;count($inputArr)>$i;$i++)
            foreach ($inputArr[$i] as $key => $value)
                if (!in_array($key, $this->rows))
                    $this->rows[]=$key;
        $this->out .= $this->headeRs();
        $this->out .= $this->body($inputArr);    
        return $this->out;
    }

}

$tableManager = new tableMaker();


function tT ($inputArray) {
    return $GLOBALS['tableManager']->r($inputArray);
}

/*

A tabla builder egyszeruen array el appellalo  html tabla keszito. 





miert ? 
Mert a symfony ba sokan hasznaljak a table bundle-t (https://packagist.org/packages/emc/table-bundle) es ugy gondoltam ez jol jonn egy ilyen mini projectbe.

*/



