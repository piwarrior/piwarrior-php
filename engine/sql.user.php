<?php

class userSQL extends abstractSQL {

    public $sql;
    public $config;
    public $chars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM01234567890";
    public $time = "";

    public function timeStamp() {
        return round(microtime(true));
    }

    public function getSalt($salt = 1) {
        $o = "";
        for ($i = 0; $salt > $i; $i++)
            $o .= substr($this->chars, rand(0, strlen($this->chars)), 1);

        return $o;
    }

    public function getId($name) {
        return $this->getSalt(2) . $name . $this->getSalt(2);
    }

    public function passwordHash($password) {
        return hash('sha512', hash('sha1', hash('sha512', hash('sha1', $password))));
    }

    public function insert() {
    }

    public function update() {
        
    }

    public function select() {
        
    }

    public function query($query) {
        $rows = [];
        $res = $this->sql->query($query);
        if ($this->sql->errno)
            debug("QUERY : || " . $query . " || " . $this->sql->error, 6);
        if ($res == false)
            return [];
        while ($row = $res->fetch_assoc())      
            $rows[] = $row;
        
        return $rows;
    }

    public function justquery($query) {
        $res = $this->sql->multi_query($query);
        if ($this->sql->errno)
            debug("JUSTQUERY : !!  " . $query . " !! " . $this->sql->error, 6);
    }

    public function security($string) {
        return htmlspecialchars($string, ENT_QUOTES);
    }

    protected function connect() {
        $this->sql = new mysqli('127.0.0.1', $this->config['user'], $this->config['password'], $this->config['db']);
        if ($this->sql->connect_errno)
            die("die die die fucking process just died!");
    }

    public function getSerial($name) { // inside serializer. Better if we do with care
        $name = $this->security($name);
        $result = $this->query("SELECT * FROM `" . $this->config['db'] . "`.`serial` WHERE name = '" . $name . "' ");
        if ((isset($result[0]['val'])) && (isset($result[0]['val']))) { // future securit check
            $serial = $result[0]['val'] + 1;
            $this->justquery("UPDATE `" . $this->config['db'] . "`.`serial` SET `val` = '" . $serial . "' WHERE `name` = '" . $name . "'");
        } else {
            $serial = 0;
            $this->justquery("INSERT INTO `" . $this->config['db'] . "`.`serial` (`name`, `val`) VALUES ('" . $name . "', '0')");
        }
        return $serial;
    }

    public function loginLogin($userid, $clientId, $sessionId) {
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersLogin` ";
        /*        serial 	id 	email 	password 	login 	phone 	inviteId 	enabled 	authed 	trackingId 	saltBefore 	saltAfter */
        $sql .= "(`id`, `userId`, `clientId`, `secret`, `loginTimestamp`) VALUES ";
        $sql .= "('" . $this->getSalt(2) . $this->getSerial('loginId') . $this->getSalt(2) . "','" . $userid . "','" . $clientId . "','" . $sessionId . "', '" . $this->time . "') ";
        $this->justquery($sql);
    }

    public function loginLogout($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `active`='0' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function loginUpdate($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `lastStamp`='" . $this->time . "' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function passwordCheck($userId, $password) {
        $userId = $this->security($userId);
        $password = $this->security($password);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersPasswd` WHERE id = '" . $userId . "'";
        $result = $this->query($sql);
        if (isset($result[0])) {
            if ($this->passwordHash($result[0]['saltBefore'] . $password . $result[0]['saltAfter']) == $result[0]['password']) {
                return true;
            } else 
                return false;
        } else
            return false;
    }

    public function passwordChange($userId, $password) {
        $userId = $this->security($userId);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersPasswd` SET `password`='" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', `saltBefore` = '" . $saltBefor . "', `saltAfter` ='" . $saltAfter . "' WHERE `id`='" . $userId . "'";
        $this->justquery($sql);
    }

    public function userFree($username, $email) {
        $out = 0;
        $email = $this->security($email);
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersPasswd` WHERE `username`='" . $username . "' OR `email`='" . $email . "' ";
        $result = $this->query($sql);
        for ($i = 0; count($result) > $i; $i++) {
            if ($username == $result[$i]['username']) {
                if ($out == 0) {
                    $out = 1;
                } else
                    $out = 3;
            }
            if ($email == $result[$i]['email']) {
                if ($out == 0) {
                    $out = 2;
                } else
                    $out = 3;
            }
        }
        return $out;
    }

    public function userList() {
        $sql = "SELECT `id`,`login`,`email` FROM `" . $this->config['db'] . "`.`usersPasswd` ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result;
        return "none";
    }

    public function userGetId($username) {
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersPasswd` WHERE `login`='" . $username . "' OR `email`='" . $username . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result[0]['id'];
        return "none";
    }
    public function userGetAll($userId) {
        $userId = $this->security($userId);
        $sql = "SELECT ". 
               "`usersPasswd`.`id` AS `userId`, `usersPasswd`.`email` AS `email`, `usersPasswd`.`login` AS `login`, ".
               "`usersPasswd`.`phone` AS `phone`, `usersPasswd`.`enabled` AS `enabled`, `usersPasswd`.`authed` AS `authed`, ".
               "`usersPasswd`.`disabled` AS `disabled`, `usersPasswd`.`admin` AS `admin`, `usersProfile`.`nickName` AS `nickName` ".                
               " FROM `" . $this->config['db'] . "`.`usersPasswd` AS `usersPasswd` LEFT JOIN `" . $this->config['db'].
               "`.`usersProfile` AS `usersProfile` ON `usersPasswd`.`id` = `usersProfile`.`userId` WHERE `usersPasswd`.`id`='".
                $userId . "'  ";       
        $result = $this->query($sql);
        if ((isset($result[0])))
            return $result[0];
        return "none";
    }
        


    public function userReg($username, $email, $password) {
        $username = $this->security($username);
        $email = $this->security($email);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $profileId = $this->getSalt(2) . $this->getSerial('profileId') . $this->getSalt(2);
        $userId = $this->getSalt(2) . $this->getSerial('userId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersProfile` ";
        $sql .= "(`id`,	`userId`, `nickName`,`avatar`,`profile`) VALUES ";
        $sql .= "('" . $profileId . "', '" . $userId . "', '" . $username . "', '', '')";
        $this->justquery($sql);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersPasswd` ";
        $sql .= "(`id`, `email`, `password`, `login`, `enabled`, `authed`, `saltBefore`, `saltAfter`) VALUES ";
        $sql .= "('" . $userId . "', '" . $email . "', '" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', '" . $username . "', '0', '0', '" . $saltBefor . "', '" . $saltAfter . "')";
        $this->justquery($sql);
        return $userId;
    }

    public function loginAdminLogin($userid, $clientId, $sessionId) {
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersLogin` ";
        /*        serial 	id 	email 	password 	login 	phone 	inviteId 	enabled 	authed 	trackingId 	saltBefore 	saltAfter */
        $sql .= "(`id`, `userId`, `clientId`, `secret`, `loginTimestamp`) VALUES ";
        $sql .= "('" . $this->getSalt(2) . $this->getSerial('loginId') . $this->getSalt(2) . "','" . $userid . "','" . $clientId . "','" . $sessionId . "', '" . $this->time . "') ";
        $this->justquery($sql);
    }

    public function loginAdminLogout($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `active`='0' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function loginAdminUpdate($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `lastStamp`='" . $this->time . "' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function passwordAdminCheck($userId, $password) {
        $userId = $this->security($userId);
        $password = $this->security($password);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE userId = '" . $userId . "'";
        $result = $this->query($sql);
        if (isset($result[0])) {
            if ($this->passwordHash($result[0]['saltBefore'] . $password . $result[0]['saltAfter']) == $result[0]['passwd']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function passwordAdminChange($userId, $password) {
        $userId = $this->security($userId);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersAdminsPasswd` SET `usersPasswd`='" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', `saltBefore` = '" . $saltBefor . "', `saltAfter` ='" . $saltAfter . "' WHERE `userId`='" . $userId . "'";
        $this->justquery($sql);
    }

    public function userAdminFree($username, $email) {
        $email = $this->security($email);
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE `login`='" . $email . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return false;
        return true;
    }

    public function userAdminList() {
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result;
        return "none";
    }

    public function userAdminGetId($username) {
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE `login`='" . $username . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result[0]['id'];
        return "none";
    }

    public function userAdminReg($username, $password) {
        $username = $this->security($username);
        $email = $this->security($email);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $userId = $this->getSalt(2) . $this->getSerial('userId') . $this->getSalt(2);
        $this->justquery($sql);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersAdminsPasswd` ";
        $sql .= "(`id`, `password`, `login`, `saltBefore`, `saltAfter`) VALUES ";
        $sql .= "('" . $userId . "', '" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', '" . $username . "', '" . $saltBefor . "', '" . $saltAfter . "')";
        $this->justquery($sql);
        return $userId;
    }

    public function adminAdd($userId) {
        $userId = $this->security($userId);
        $adminId = $this->getSalt(2) . $this->getSerial('adminId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`admins` ";
        $sql .= "(`id`, `userId`, `creationStamp`) VALUES ";
        $sql .= "('" . $adminId . "', " . $userId . "',  '" . $this->time . "')";
        $this->justquery($sql);
        return $adminId;
    }

    public function adminCheck($userId) {
        $userId = $this->security($userId);
        $sql = "SELECT `admin` FROM `" . $this->config['db'] . "`.`usersPasswd`  WHERE `id` = '" . $userId . "' AND `admin` = '1' ";
        $result = $this->query($sql);
        if (count($result) > 0)
            return true;
        return false;
    }

    public function adminDel($userId) {
        $userId = $this->security($userId);
        $sql = "DELETE FROM `" . $this->config['db'] . "`.`admins` WHERE `userId` = '" . $userId . "' ";
        $this->justquery($sql);
    }

    public function adminList() {
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`admins` ";
        $result = $this->query($sql);
    }

    public function whmcsAdd($userId, $whmcsuser, $whmcsuserid, $whmcsmd5) {
        $userId = $this->security($userId);
        $whmcsId = $this->getSalt(2) . $this->getSerial('whmcsId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`whmcs` ";
        $sql .= "(`id`, `userId`, `whmcsuser`, `whmcsuserid`, `whmcsmd5`) VALUES ";
        $sql .= "('" . $whmcsId . "', '" . $userId . "',  '" . $whmcsuser . "', '" . $whmcsuserid . "', '" . $whmcsmd5 . "')";
        $this->justquery($sql);
        return $whmcsId;
    }

    public function whmcsRead($userId) {
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`whmcs`  WHERE `userId` = '" . $userId . "'";
        $result = $this->query($sql);
        if (isset($result[0]))
            return $result[0];
        return [];
    }

    public function whmcsDel() {
        $userId = $this->security($userId);
        $sql = "DELETE FROM `" . $this->config['db'] . "`.`whmcs` WHERE `userId` = '" . $userId . "' ";
        $this->justquery($sql);
    }

    public function ticketThreadNewList() {
        $sql = "SELECT * FROM `ticketThread` WHERE status = 'new'";
        $result = $this->query($sql);
        return $result;
    }

    public function ticketThreadList() {
        $sql = "SELECT * FROM `ticketThread`";
        $result = $this->query($sql);
        return $result;
    }

    public function ticketThreadChange($threadId, $status) {
        $threadId = $this->security($thsreadId);
        $status = $this->security($status);
        $sql = "UPDATE `" . $this->config['db'] . "`.`ticketThread` SET `status`='" . $status . "' WHERE `id`='" . $threadId . "'";
        $this->justquery($sql);
    }

    public function ticketThreadMake($usedId, $typeId, $requesterEmail, $requesterName, $title) {
        $userId = $this->security($userId);
        $typeId = $this->security($typeId);
        $ticketThreadId = $this->getSalt(2) . $this->getSerial('ticketThreadId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`ticketThread` ";
        $sql .= "(`id`, `status`, `creationStamp`, `userId`, `typeId`, `requesterEmail`, `requesterName`, `title`) VALUES ";
        $sql .= "('" . $ticketThreadId . "', 'new', '" . $this->time . "', '" . $userId . "', '" . $typeId . "', '" . $requesterEmail . "', '" . $requesterName . "', '" . $title . "')";
        $this->justquery($sql);
        return $ticketThreadId;
    }

    public function ticketList($threadId) {
        $threadId = $this->security($threadId);
        $sql = "SELECT * FROM `ticket` WHERE `threadId` = '" . $threadId . "' ";
        return $this->query($sql);
    }

    public function ticketMake($threadId, $type, $userId, $message) {
        $threadId = $this->security($threadId);
        $type = $this->security($type);
        $userId = $this->security($userId);
        $message = $this->security($message);
        $ticketId = $this->getSalt(2) . $this->getSerial('ticketId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`ticket` ";
        $sql .= "(`id`, `type`, `threadId`, `userId`,`message`, `creationStamp`) VALUES ";
        $sql .= "('" . $ticketId . "', '" . $type . "', '" . $threadId . "', '" . $userId . "', '" . $nessage . "', '" . $this->time . "')";
        $this->justquery($sql);
        return $ticketId;
    }

    public function productAdd($name, $typeId) {
        $name = $this->security($name);
        $typeId = $this->security($typeId);
        $id = $this->getSalt(2) . $this->getSerial('product') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`products` ";
        $sql .= "(`id`, `name` `typeId`) VALUES ";
        $sql .= "(`" . $id . "`, `" . $name . "`, `" . $typeId . "`)";
        $this->justquery($sql);
        return $id;
    }

    public function productTypeDetailsGet($typeId) {
        $typeId = $this->security($typeId);
        $sql = "SELECT `id` FROM  `" . $this->config['db'] . "`.`products_details_type` WHERE `products_typeId` = '" . $typeId . "'";
        return $this->query($sql);
    }

    public function langStringAutoAdd($string) {
        $string = $this->security($string);
        $sql = "SELECT `id` FROM  `" . $this->config['dbs']['langueges'] . "`.`strings` WHERE `arrayString` = '" . $string . "'";
        $results = $this->query($sql);
        if (count($results) < 1) {
            $id = $this->getSalt(2) . $this->getSerial('langStrings') . $this->getSalt(2);
            $sql = "INSERT INTO `" . $this->config['dbs']['langueges'] . "`.`strings` (id, arrayString) SELECT '" . $id . "', '" . $string . "'  WHERE NOT EXISTS (SELECT `id` FROM  `" . $this->config['dbs']['langueges'] . "`.`langStrings` WHERE `arrayString` = '" . $string . "') LIMIT 1";
            $this->justquery($sql);
            return $id;
        }
    }

    public function langStringAddArray($array) {
        if (1 > count($array))
            return false;
        $sql = "INSERT INTO `" . $this->config['dbs']['langueges'] . "`.`strings`  ";
        $sql .= "(`id`, `arrayString`) VALUES ";
        for ($i = 0; count($array) > $i; $i++) {
            $id = $this->getSalt(2) . $this->getSerial('langStrings') . $this->getSalt(2);
            if ($i > 0)
                $sql .= ",";
            $sql .= " ('" . $id . "', '" . $array[$i] . "')";
        }
        $this->justquery($sql);
    }

    public function langStringGetAll() {
        $out = [];
        $sql = "SELECT * FROM  `" . $this->config['dbs']['langueges'] . "`.`strings` ";
        $results = $this->query($sql);
        for ($i = 0; count($results) > $i; $i++)
            $out[] = $results[$i]['arrayString'];
        return $out;
    }


    public function __construct() {
        global $super_config;
        $this->config = $super_config['sql'];
        $this->connect();
        $this->time = time();
    }

    public function __destruct() {
        
    }

}
