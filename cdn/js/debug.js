

setTimeout(function(){
var debug = function(){
    this.status=false;
    this.open= function(){
        document.getElementById('debug').style.display = "block";
        document.getElementById('debugButton').style.display = "none";
        that.status=true;
        sessionStorage.setItem('pandoraDebug', '1');
    }
    this.close = function (){
        document.getElementById('debug').style.display = "none";
        document.getElementById('debugButton').style.display = "block";
        that.status=false;
        sessionStorage.setItem('pandoraDebug', '0');
    } 
    this.switch=function(){
         if(that.status){
             that.close();
         }else{
             that.open();
         }
    }
    this.init = function (){
        if (sessionStorage.getItem('pandoraDebug')==='1'){
                that.open();
        }else{
        that.close();
        document.getElementById('debugButton').addEventListener("click", that.open);
        }
    }
    var that = this;
    this.init();
}
var debugRun = new debug();
}, 1000);


