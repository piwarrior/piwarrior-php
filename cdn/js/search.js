
var searchManagerClass = function (){
    this.busy=0;
    this.last="";
    this.element=document.getElementById("searchInput");
    this.releaseTimeout=1;
    this.release = function(){
        if (this.releaseTimeout != 1)
            clearTimeout(this.releaseTimeout);
        this.releaseTimeout=setTimeout(function(){
             that.busy = 0;
             that.change ();
             that.releaseTimeout=1
        }, 200);
    }
    this.freeCheck=function (){
         if(that.busy == 0)
            return true;
         return false;
    }
    this.change=function(){
        var current = that.element.value;
        if (current == that.last)
            return false;
        if (3>current.length)
            return false;
        if(!that.freeCheck())
            return false;
        that.busy = 1;
        setTimeout(that.search, 1000);
    }
    this.search=function(){
        var current = that.element.value;
        that.last   = current; 
        if (window.XMLHttpRequest) {
            var xmlhttp = new XMLHttpRequest();
        } else {
            var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () { 
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
              //  var incomming = JSON.parse(xmlhttp.responseText.toString());
              console.log(xmlhttp.responseText.toString());
            }
        }
        var obj = {
            c:"rpf",
            api:"api",
            t:current
        }
        var urlParam =  Object.keys(obj).map(function(key){
            return key + '=' + obj[key]}).join('&');
        xmlhttp.open("POST", "/", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(urlParam);
        that.release();
    }

    var that = this;

}




var searchManager = new searchManagerClass();
window.onload=function(){
     document.getElementById('searchInput').addEventListener('keyup',  function(){
        searchManager.element=document.getElementById('searchInput');
        searchManager.change();
     }, true);
}



