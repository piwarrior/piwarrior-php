<?php




class userSQL extends abstractSQL {
    public function userFree($user, $email){
        $result = 0;
        if(!isset($this->qf('userFree', [$user])[0])) $result++;
        if(!isset($this->qf('userFree', [$email])[0])) $result+=2;
        return $result;
    }
    public function loginLogin($userId, $clientId, $sessionId) {
        $loginId =  $this->getId('profileId');
        $this->qf('userLoginLogin', [$loginId, $userId, $clientId, $sessionId]);
        return $loginId;
    }

    public function loginLogout($sessionId) {
        $this->qf('userLogout', [$sessionId]);
    }

    public function loginUpdate($sessionId) {
        $this->qf('userLoginUpdate', [$sessionId]);
    }

    public function passwordCheck($login, $password) {
        $result = $this->qf('passwordCheck', [$login, $password]);
        if ($result==NULL)
            return false;
        return $result;
    }

    public function passwordChange($login, $passwordOld, $passwordNew) {
        $result = $this->qf('passwordChange', array($login ."','". $passwordOld. "','". $passwordNew));
        return $result[0]['result'];
    }

    public function userExist($username, $email, $phone) {
        return $this->qf('userExistCheck', array($username, $email, $phone));
    }

    public function userExistAll($username) {
        $result = $this->qf('userExistCheckAll', array($username));
        return $result[0]['exist'];
    }


    public function userExistCom($username, $email, $phone) {
        $result = $this->qf('userExistCheckCom', array($username, $email, $phone));
        return $result[0]['exist'];
    }

    public function userList() {
        $sql = "SELECT `id`,`login`,`email` FROM `" . $this->config['dbs']['user'] . "`.`usersPasswd` ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result;
        return "none";
    }

    public function userGetId($username) {
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersPasswd` WHERE `login`='" . $username . "' OR `email`='" . $username . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result[0]['id'];
        return "none";
    }
    public function userGetAll($userId) {
        $userId = $this->security($userId);
        $sql = "SELECT ". 
               "`usersPasswd`.`id` AS `userId`, `usersPasswd`.`email` AS `email`, `usersPasswd`.`login` AS `login`, ".
               "`usersPasswd`.`phone` AS `phone`, `usersPasswd`.`enabled` AS `enabled`, `usersPasswd`.`authed` AS `authed`, ".
               "`usersPasswd`.`disabled` AS `disabled`, `usersPasswd`.`admin` AS `admin`, `usersProfile`.`nickName` AS `nickName` ".                
               " FROM `" . $this->config['db'] . "`.`usersPasswd` AS `usersPasswd` LEFT JOIN `" . $this->config['db'].
               "`.`usersProfile` AS `usersProfile` ON `usersPasswd`.`id` = `usersProfile`.`userId` WHERE `usersPasswd`.`id`='".
                $userId . "'  ";       
        $result = $this->query($sql);
        if ((isset($result[0])))
            return $result[0];
        return "none";
    }
        


        
    public function userReg($username, $email, $phone, $password) {
//        $username = $this->security($username);
//        $email = $this->security($email);
//        $phone = $this->security($phone);
//        $password = $this->security($password);
        $saltBefore = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $profileId =  $this->getId('profileId');
        $userId =  $this->getId('userId');
        if (0 == $this->qf('userRegister', array($username, $email, $phone, $password, $saltBefore, $saltAfter, $userId, $profileId)))
            return $userId;
      
#        $sql = "SELECT  `" . $this->config['dbs']['user'] . "`.`userRegister`('". $username. "','". $email. "','". $phone. "','".$password. "','". $saltBefore. "','". $saltAfter. "','". $userId. "','". $profileId. "' ) AS error";
#        $result = $this->query($sql);
#        if ($result[0]['error'] == 0){
#            return $userId;
#        }elseif($result[0]['error'] == 1){
#            
#        }
        return false;
    }

    public function loginAdminLogin($userid, $clientId, $sessionId) {
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersLogin` ";
        /*        serial 	id 	email 	password 	login 	phone 	inviteId 	enabled 	authed 	trackingId 	saltBefore 	saltAfter */
        $sql .= "(`id`, `userId`, `clientId`, `secret`, `loginTimestamp`) VALUES ";
        $sql .= "('" . $this->getSalt(2) . $this->getSerial('loginId') . $this->getSalt(2) . "','" . $userid . "','" . $clientId . "','" . $sessionId . "', '" . $this->time . "') ";
        $this->justquery($sql);
    }

    public function loginAdminLogout($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `active`='0' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function loginAdminUpdate($sessionId) {
        $sessionId = $this->security($sessinId);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersLogin` SET `lastStamp`='" . $this->time . "' WHERE `secret`='" . $sessioId . "'";
        $this->justquery($sql);
    }

    public function passwordAdminCheck($userId, $password) {
        $userId = $this->security($userId);
        $password = $this->security($password);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE userId = '" . $userId . "'";
        $result = $this->query($sql);
        if (isset($result[0])) {
            if ($this->passwordHash($result[0]['saltBefore'] . $password . $result[0]['saltAfter']) == $result[0]['passwd']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function passwordAdminChange($userId, $password) {
        $userId = $this->security($userId);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $sql = "UPDATE `" . $this->config['db'] . "`.`usersAdminsPasswd` SET `usersPasswd`='" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', `saltBefore` = '" . $saltBefor . "', `saltAfter` ='" . $saltAfter . "' WHERE `userId`='" . $userId . "'";
        $this->justquery($sql);
    }

    public function userAdminFree($username, $email) {
        $email = $this->security($email);
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE `login`='" . $email . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return false;
        return true;
    }

    public function userAdminList() {
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result;
        return "none";
    }

    public function userAdminGetId($username) {
        $username = $this->security($username);
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`usersAdminsPasswd` WHERE `login`='" . $username . "' ";
        $result = $this->query($sql);
        if ((isset($result[0])) && (isset($result[0]['id'])))
            return $result[0]['id'];
        return "none";
    }

    public function userAdminReg($username, $password) {
        $username = $this->security($username);
        $email = $this->security($email);
        $saltBefor = $this->getSalt(20);
        $saltAfter = $this->getSalt(20);
        $userId = $this->getSalt(2) . $this->getSerial('userId') . $this->getSalt(2);
        $this->justquery($sql);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`usersAdminsPasswd` ";
        $sql .= "(`id`, `password`, `login`, `saltBefore`, `saltAfter`) VALUES ";
        $sql .= "('" . $userId . "', '" . $this->passwordHash($saltBefor . $password . $saltAfter) . "', '" . $username . "', '" . $saltBefor . "', '" . $saltAfter . "')";
        $this->justquery($sql);
        return $userId;
    }

    public function adminAdd($userId) {
        $userId = $this->security($userId);
        $adminId = $this->getSalt(2) . $this->getSerial('adminId') . $this->getSalt(2);
        $sql = "INSERT INTO `" . $this->config['db'] . "`.`admins` ";
        $sql .= "(`id`, `userId`, `creationStamp`) VALUES ";
        $sql .= "('" . $adminId . "', " . $userId . "',  '" . $this->time . "')";
        $this->justquery($sql);
        return $adminId;
    }

    public function adminCheck($userId) {
        $userId = $this->security($userId);
        $sql = "SELECT `admin` FROM `" . $this->config['db'] . "`.`usersPasswd`  WHERE `id` = '" . $userId . "' AND `admin` = '1' ";
        $result = $this->query($sql);
        if (count($result) > 0)
            return true;
        return false;
    }

    public function adminDel($userId) {
        $userId = $this->security($userId);
        $sql = "DELETE FROM `" . $this->config['db'] . "`.`admins` WHERE `userId` = '" . $userId . "' ";
        $this->justquery($sql);
    }

    public function adminList() {
        $sql = "SELECT * FROM `" . $this->config['db'] . "`.`admins` ";
        $result = $this->query($sql);
    }

}




