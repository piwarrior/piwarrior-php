<?php
#INC sql


class userManager {
     private $sql;
     public function registration($user, $email, $phone, $passwordA, $passwordB){
         if (!isset($user) || (isset($user) && $user == ""))
             tRa(__('Missing username parameter.'));
         if (!isset($email) || (isset($email) && $email == ""))
             tRa(__('Missing  e-mail parameter'));
         if (!isset($email) || (isset($email) && $email == ""))
             tRa(__('Missing phone parameter'));
         if ((!tR())&&($this->sql->userExist($user, $email, $phone)>0)){
             tRa(__('User already exist.'));
         }
         if (!isset($passwordA) || (isset($passwordA) && $passwordA == "")) {
             tRa(__('Missing password parameter.'));
         } elseif ($passwordA != $passwordB)
             tRa(__('Password does not match.'));
         if( tRt()) return false;
         $userId = $this->sql->userReg($user, $email, $phone, $passwordA);
         return true;
     }
     private function registrationForm(){
         mV('user','registration.form');
     }
     private function registrationCL($argv){
         if($this->registration($argv[2], $argv[3], $argv[4], $argv[5], $argv[6])==true)return mV('user','registration.success');
         mV('user','registration.form');
     }
     private function registrationSend($user, $email, $phone, $passwordA, $passwordB){
         if($this->registration($user, $email, $phone, $passwordA, $passwordB)==true)return mV('user','registration.success');
         mV('user','registration.form');
     }
     private function loginForm(){
         mV('user', 'login.form');
     }
     private function kickCL($argv){}
     private function banCL(){}
     public function cliSide($input){
         if($input[0]=="registration") return $this->registrationCL($argv); 
         if($input[0]=="kick") return $this->kickCL($argv); # not implemented 
         if($input[0]=="ban") return $this->banCL($argv); # not implemented
         return false;
     }
     public function outSideApi($input){
         if($input=="login")
             return $this->loginApi(iG('login'),iG('password'));
         return false;
     }
     public function outSide($input){
         if($input=="login")
             return $this->login(iG('login'),iG('password'));
         if($input=="loginForm")
            return $this->loginForm();
         if($input=="registrationSend") 
            return $this->registrationSend(iG('user'), iG('email'), iG('phone'), iG('passwordA'), iG('passwordB'));
         if($input=="registrationForm")
            return $this->registrationForm();
         return false;
     }
     public function inSide($input){
         if($input=="logoutConfirm") return  mV('user', 'logout');
         if($input=="logout") return  $this->logout();
         return false;
     }
     public function logout(){
         $GLOBALS['loginManager']->logout();
         mV('user', 'logining');
     }
     public function login($login,$password){
        $userId = $this->sql->passwordCheck($login,$password);
        if(!$userId){
            tRa(__('The email address or password you entered was not recognized.'));
            $this->loginForm();
            return false;
        }
        $loginId=$this->sql->loginLogin($userId, sG('clientId'), session_id());
        if($loginId==false){ 
            tRa(__('The email address or password you entered was not recognized.'));
            return false;
        }
        sS('loginId', $loginId);
        mV('user', 'logining');
        return true; 
     }
     public function loginApi($login,$password){
        $userId = $this->sql->passwordCheck($login,$password);
        if(!$userId){
            oA('login', 'failed');
            return false;
        }
        $loginId=$this->sql->loginLogin($userId, sG('clientId'), session_id());
        if($loginId==false){ 
            oA('login', 'Internal error');
            return false;
        }
        sS('loginId', $loginId);
        oA('login', 'Success');
        return true; 
     }
     public function loginStatus(){

     }
     public function __construct($dbc){
         $this->sql=new userSQL();
         $this->sql->use($dbc);
     }
}

$user = new userManager($super_config['sql']['db']);




