<?php


    hC("css/user.css");
    o(tE('div', ["class"=>"popupTitle"],  __('Are you sure you want to log out?')));
    o(tE('div', ['class'=>"popupTwoButtonLine"],
        tE('div', ['class'=>"popupTwoButtonHalf"], tF([
                "method"=>"post"
            ],[[
                    "type" =>"submit",
                    "value"=>__('No')
            ]])).
        tE('div', ['class'=>"popupTwoButtonHalf"], tF([
                    "method"=>"post"
                ],[[
                    "type" =>"hidden",
                    "name" =>"c",
                    "value"=>"uo"
                ],[
                    "type" =>"submit",
                    "value"=>__('Yes')
                ]
            ])) 
    ));


