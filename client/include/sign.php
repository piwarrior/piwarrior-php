<?php



class SIGNATURE {
    private $key; 
    private $packet=[];
    private $timeStamp;
    public function clean(){
        $this->timeStamp=time();
        $this->packet=[];
    }
    public function add($value,$id){
        $newPacket=[];
        $newPacket['timestamp']=$this->timeStamp;
        $newPacket['id']=$id;
        $newPacket['value']=$value;
        $this->packet[]=$newPacket;
    }
    public function get(){
        $packet=base64_encode(json_encode($this->packet));
        $signature=hash("sha512,3", $packet.$this->key, false);
        return ("packet=".$packet."&signature".signature);
    }
    public function __construct() {
        global $SUPERGLOBAL;
        $this->key = $SUPERGLOBAL['config']['app']['key'];
    }


}


