<?php
class CPULOAD{
    private $cpusLastData=false;
    private $cpusStatData=false;
    private $cpusCurrentData=false;
    private $cpusTime=false;
    public $cpusLoad=false;
    private function getCpusLoad() {
        if (!is_readable("/proc/stat")) 
            return false;
        $stats = @file_get_contents("/proc/stat");
        if ($stats == false) 
            return false;
        $stats = preg_replace("/[[:blank:]]+/", " ", $stats);
        $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
        $stats = explode("\n", $stats);
        $this->cpusLastData=$this->cpusCurrentData;
        foreach ($stats as $statLine) {
            $statLineData = explode(" ", trim($statLine));
            if ((count($statLineData) >= 5) &&($statLineData[0] == "cpu")) {
                $this->cpusCurrentData=array(
                    $statLineData[1],
                    $statLineData[2],
                    $statLineData[3],
                    $statLineData[4],
                );
                return true;
            }
        }
        return false;
    }
    private function calculation(){
        $this->cpusTime=0;
        for ($i = 0; $i<4;$i++) $this->cpusStatData[$i]=$this->cpusCurrentData[$i]-$this->cpusLastData[$i];
        for ($i = 0; $i<4;$i++) $this->cpusTime+=$this->cpusStatData[$i];
        $this->cpusLoad = 100 - ($this->cpusStatData[3] * 100 / $this->cpusTime);
    }
    public function get() {
        if (!is_readable("/proc/stat"))
            return false;
        if (!$this->getCpusLoad())
            return false;
        if (($this->cpusCurrentData == false) || ($this->cpusLastData==false)) 
            return false;
        $this->calculation();
        return true;
    }
    public function test() {
       for($i=0;$i<1000;$i++){
           $this->get();
           echo("\n");
           print_r($this->cpusLastData);
           print_r($this->cpusStatData);
           print_r($this->cpusCurrentData);
           print_r($this->cpusLoad);
           sleep(1);
       }
    }
}

