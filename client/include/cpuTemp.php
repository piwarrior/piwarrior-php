<?php 


#timeStamp=$(date +%s)
#cpuTemp0=$(cat /sys/class/thermal/thermal_zone0/temp)
#cpuTemp1=$(($cpuTemp0/1000))
#cpuTemp2=$(($cpuTemp0/100))
#cpuTempM=$(($cpuTemp2 % $cpuTemp1))
#rrdtool update cpu.rrd $timeStamp:$cpuTemp1 &


class CPUTEMP {
    private $file;
    public $orginalValue = 0;
    public $tempFloat = 0;
    public $temp=0;
    private function calculate(){
         $this->tempFloat=(($this->originalValue/1000)%($this->originalValue));
         $this->temp=round($this->tempFloat); 
    }
    private function read (){
         $this->file=fopen("/sys/class/thermal/thermal_zone0/temp", 'r');
         $this->originalValue=fgets($this->file);
         fclose($this->file);
    }
    public function get(){
         $this->read();
         $this->calculate();
    }
    public function test(){
         $this->get();
         echo("\n temp test \n");
         echo($this->originalValue."\n");
         echo($this->tempFloat."\n");
         echo($this->temp."\n");
    }
}







